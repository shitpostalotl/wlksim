class thing():
	def __init__(self,icon,text,key,auto):
		self.icon = icon
		self.text = text
		self.key = key
		self.auto = auto
	def __str__(self): return self.icon
	def __len__(self): return len(self.icon)
	def interact(self,player):print(self.text)

class door(thing):
	def __init__(self,icon,text,key,auto, destination):
		super().__init__(icon,text,key,auto)
		self.destination = destination
	def interact(self, player):
		super().interact(player)
		player.cords = self.destination[:2]
		player.room = self.destination[2]
