#! /usr/bin/env nix-shell
#! nix-shell -i python3
import utils, printroom, player, thing, os, copy, pickle

dfile = open("game","rb")
data = pickle.load(dfile)
dfile.close()
p = data[0]
r = data[1]
t = data[2]

if (os.path.exists("save")):
	pfile = open("save","rb")
	p = pickle.load(pfile)
	pfile.close()
else: p.settings(t)
interact = False

while True:
	print('\033c\x1b[?25l' + printroom.roomtostr(printroom.crop(printroom.format(copy.deepcopy(r[p.room]), p.color), p.cords, min(os.get_terminal_size())),p.color[1]) + printroom.addplayer(p.cords,p.icon,p.color[0],min(os.get_terminal_size()),r[p.room]) + "\033[0m\033[0;0H", end="")
	if(interact):
		focus.interact(p)
		if not([p.room,focusCords[0],focusCords[1]] in p.flags): p.flags.append([p.room,focusCords[0],focusCords[1]])
		interact = False
	key = utils.getkey()
	try: p.move(p.controlls[key],r[p.room])
	except: pass
	focusCords = [max(min(len(r[p.room])-1,p.cords[1]+p.face[1]),0),max(min(len(r[p.room][0])-1,p.cords[0]+p.face[0]),0)]
	focus = r[p.room][focusCords[0]][focusCords[1]]
	if (not isinstance(focus, str)): interact = (key==p.controlls['use'] or focus.auto) and (focus.key == ["","",""] or focus.key in p.flags) and isinstance(focus, thing.thing)
	if(key==p.controlls['set']): p.settings(t)
	elif(key==p.controlls['xit'] and input(t["quitConf"]+" ").lower()==t["quitConfKey"]):
		dfile = open("save", "wb")
		pickle.dump(p,dfile)
		dfile.close()
		print('\033c')
		exit()
