	i. general

		A. game world structure

			- 2D lists represent rooms, 1 char/door/"thing" per list index

			- 2D lists stored in one other bigger list

			- room scenery represented w/ ascii art stored as chars in list

			- "thing"s as objects in list, char representation & dialog to be shown on interaction

			- index zero of "world list" = player name, color-scheme, player char, location, facing, flags, keybinds

			- pickled for storage (has to be, storing custom objects)

		B. loop

			- player is a custom color, non-walkable environment is bold (opposed to non-bold walkable environment), "thing"s are other custom color

			- colors (& name & keybinds) asked asked at beginning of game

			- for each turn, take input key, execute associated action

			- print screen

	ii. objects

		A. player

			- [X] char

			- [X] color

			- [X] location

			- [X] facing

			- [X] read defaults from editor

		B. "thing"s

			- [X] location represented by placement in room list

			- [X] color stored in save

			- [X] text description

			- [X] flags

		C. background char

			- [X] string

			- [X] un-walkable if more then one char

			- [X] only first char displayed

		D. door

			- [X] subclass of "thing"

			- [X] exit location

			- [X] flag to unlock

			- [X] automatic (do you need to interact w/ them to use)

	iv. polish

		A. main

			- [X] everything done by interact, do w/ function in 'thing'

			- [X] add player to screen via escape code "cursor to location"

			- [X] os.system clears to escape code clears

			- [X] color selection via loop in loop instead of monoline bullshit

			- [X] settings at any time

			- [X] "UI" text to world file

			- [X] save flag state in player attribute

			- [X] player printing account for crop (crop isnt broken)

			- [X] roomlist to roomdict

		B. level editor (UI)

			- [X] char trait defaults 

			- [X] turn text files into room list 

			- [X] room editing screen with controllable cursor

			- [X] declare chars as "thing"s/doors/unwalkable

			- [X] add "thing" text

			- [X] add door exit locations 

			- [ ] let editor make impassible things by making it a thing first, make impassible things not