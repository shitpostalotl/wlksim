#! /usr/bin/env nix-shell
#! nix-shell -i python3
import utils, os, pickle, copy, json, player, thing, printroom

r = {}

while True:
	print("\033c[T]: UI Text\n[P]: Default game settings\n[R]: Rooms\n[Q]: Quit")
	key = utils.getkey().lower()
	if (key=="t"):
		t = {"quitConf": utils.inPre("Quit message: ", "Quit [y]?"),"quitConfKey": utils.inPre("Quit confirmation key: ", "y"),"pSelect":utils.inPre("Player char select text: ","Player Char"),"player": utils.inPre("Player color selection text: ","PLAYER"),"world": utils.inPre("World color selection text: ","WORLD"),"thing":  utils.inPre("Thing color selection text: ","THING"),"red":  utils.inPre("Red color indicator: ","R"),"green": utils.inPre("Green color indicator: ","G"),"blue": utils.inPre("Blue color indicator: ","B"),"up": utils.inPre("Up direction indicator: ","UP"),"down": utils.inPre("Down direction indicator: ","DOWN"),"left": utils.inPre("Left direction indicator: ","LEFT"),"right": utils.inPre("Right direction indicator: ","RIGHT"),"use": utils.inPre("Use settings text: ","USE"),"set": utils.inPre("Settings settings text: ","SETTINGS"),"xit": utils.inPre("Quit settings text: ","QUIT")}
	elif (key=="p"):
		p = player.player([int(input("Starting X: ")),int(input("Starting Y: "))],input("Starting room: "), "P",[[0,255,255],[0,0,0],[0,255,0]], {'w': [0, -1], 's': [0, 1], 'a': [-1, 0], 'd': [1, 0], 'use': "e", 'set': "x", "xit": "q"})
		p.settings(t)
	elif (key=="r"):
		uishow = True
		while True:
			print("\033c[A]: Add rooms from folder\n[E]: Edit rooms\n[Q]: Quit to main menu")
			key = utils.getkey().lower()
			if (key=="a"):
				for i in os.scandir(input("Path: ")):
					roomfile = open(i.path,"r")
					roomfilename = os.path.basename(roomfile.name)
					r[roomfilename] = [ list(line.rstrip("\n")) for line in roomfile ]
					roomfile.close()
					highlen = max([len(i) for i in r[roomfilename]])
					for j in r[roomfilename]:
						while len(j) < highlen: j.append(" ")
			if (key=="e"):
				cursor = player.player([0,0],input("\033c"+"\n".join(list(r.keys()))+"\n: "), "",[[0,255,255],[0,0,0],[0,255,0]], {'w': [0, -1], 's': [0, 1], 'a': [-1, 0], 'd': [1, 0]})
				while (key.lower()!="q"):
					print('\033c\x1b[?25l' + printroom.roomtostr(printroom.crop(printroom.format(copy.deepcopy(r[cursor.room]), cursor.color), cursor.cords, min(os.get_terminal_size())),cursor.color[1]) + "[I]mpassible - [T]hing - [P]assageway - [R]ead attrs - [Q]uit - [U]I toggle"*uishow + printroom.addplayer(cursor.cords,(str(r[cursor.room][cursor.cords[1]][cursor.cords[0]])*(str(r[cursor.room][cursor.cords[1]][cursor.cords[0]])!=" ")+("^"*(r[cursor.room][cursor.cords[1]][cursor.cords[0]]==" "))),cursor.color[0],min(os.get_terminal_size()),r[cursor.room]) + "\033[0m\033[0;0H", end="")
					key = utils.getkey()
					try: cursor.move(cursor.controlls[key.lower()],r[cursor.room])
					except: pass
					focus = [max(min(len(r[cursor.room])-1,cursor.cords[1]+cursor.face[1]),0),max(min(len(r[cursor.room][0])-1,cursor.cords[0]+cursor.face[0]),0)]
					if (key.lower()=="i"):
						if hasattr(r[cursor.room][focus[0]][focus[1]], "text"): r[cursor.room][focus[0]][focus[1]].text = r[cursor.room][focus[0]][focus[1]].text[0] + "."*(len(r[cursor.room][focus[0]][focus[1]].text)==1)
						else: r[cursor.room][focus[0]][focus[1]] = r[cursor.room][focus[0]][focus[1]][0] + "."*(len(r[cursor.room][focus[0]][focus[1]])==1)
					elif (key.lower()=="t" or key.lower()=="p"):
						r[cursor.room][focus[0]][focus[1]] = thing.thing(r[cursor.room][focus[0]][focus[1]],input("Text: "),[input("Key Y: "),input("Key X: "),input("\n".join(list(r.keys()))+"\nKey room: ")],input("Autorun [Y]? ").lower()=="y")
						if (key.lower()=="p"): r[cursor.room][focus[0]][focus[1]].destination=[input("Dest Y: "),input("Dest X: "),input("\n".join(list(r.keys()))+"\nDest room: ")]
					elif (key.lower()=="r" and hasattr(r[cursor.room][focus[0]][focus[1]], "text")): input(json.dumps(focus.__dict__,indent=4))
					elif (key.lower()=="u"): uishow = not uishow
			if (key=="q"):
				print('\033c')
				break
	elif (key=="q"):
		print('\033c')
		break

dfile = open("game", "wb")
pickle.dump([p,r,t],dfile)
dfile.close()
