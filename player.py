import utils

class player():
	def __init__(self,location,room, icon,color, controlls):
		self.cords = location
		self.room = room
		self.icon  = icon
		self.color = color
		self.controlls = controlls
		self.face = [1,0]
		self.flags = []
	def move(self,trans,roomlist):
		next = [self.cords[0] + trans[0], self.cords[1] + trans[1]]
		self.face = trans
		if ((len(roomlist[0])>next[0]>-1 and len(roomlist)>next[1]>-1) and len(roomlist[next[1]][next[0]])==1): self.cords = next
	def settings(self,txt):
		self.icon = utils.inPre(txt["pSelect"]+": ",self.icon)[0]
		self.color = [[int(utils.inPre([txt["player"],txt["world"],txt["thing"]][i]+" "+[txt["red"],txt["green"],txt["blue"]][j]+": ",str(self.color[i][j]))) for j in range(0,3)] for i in range(0,3)]
		defcontrolls = list(self.controlls.keys())
		self.controlls = {utils.inPre(txt["up"]+": ",defcontrolls[0]):[0,-1],utils.inPre(txt["down"]+": ",defcontrolls[1]):[0,1],utils.inPre(txt["left"]+": ",defcontrolls[2]):[-1,0],utils.inPre(txt["right"]+": ",defcontrolls[3]):[1,0],'use':utils.inPre(txt["use"]+": ",self.controlls['use']),'set':utils.inPre(txt["set"]+": ",self.controlls['set']),'xit':utils.inPre(txt["xit"]+": ",self.controlls['xit'])}
