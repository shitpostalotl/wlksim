import thing

def format(roomlist,color):
	for i in range(len(roomlist)): roomlist[i] = list(map(lambda n:("\033[38;2;{};{};{}m".format(color[2][0],color[2][1],color[2][2])*isinstance(n, thing.thing))+(str(n)*(len(n)==1))+('\033[1m'+str(n)[0]+'\033[0m')*(len(n)>1)+("\033[38;2;{};{};{}m".format(color[1][0],color[1][1],color[1][2])),roomlist[i]))
	return roomlist

def crop(roomlist, target, size):
	side = int(size/2)-2
	out = roomlist
	if (len(roomlist)>=size):
		out = roomlist[((target[1]-side)*(target[1]>=side))-((side-abs(len(roomlist)-target[1])+1)*(abs(target[1]-len(roomlist))<=side)):target[1]+side+1+(side-target[1])*(target[1]<side)] # crop U+D
		out = list(map(lambda x:x[((target[0]-side)*(target[0]>=side))-((side-abs(len(roomlist)-target[0])+0)*(abs(target[0]-len(roomlist))<=side)):target[0]+side+1+(side-target[0])*(target[0]<side)],out)) # crop L+R
	return out

def roomtostr(roomlist,color):
	out = "\033[38;2;{};{};{}m".format(color[0],color[1],color[2])
	for i in range(len(roomlist)): out += ''.join(roomlist[i]) + ("\n" * (i != len(roomlist)))
	return out

def addplayer(loc,char,color, size,roomlist):
	side = int(size/2)-2
	magic = lambda n:((len(roomlist)*(n==1)+len(roomlist[0])*(n==0))<size or ((len(roomlist)*(n==1)+len(roomlist[0])*(n==0))>=size and loc[n]<side))*(loc[n]+1)+((len(roomlist)*(n==1)+len(roomlist[0])*(n==0))>size and loc[n]>=side)*(side+1)+((len(roomlist)*(n==1)+len(roomlist[0])*(n==0))>size and loc[n]>(len(roomlist)*(n==1)+len(roomlist[0])*(n==0))-side)*(side+(loc[n]-(len(roomlist)*(n==1)+len(roomlist[0])*(n==0)))+(n==1))
	return "\033[{};{}H".format(magic(1),magic(0)) + "\033[38;2;{};{};{}m".format(color[0],color[1],color[2]) + char